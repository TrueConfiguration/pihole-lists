#!/bin/bash

# Adds aditional lists for Pi-hole.
#
# This script will download and add to the configuration files a selection of 
# adlists, commonly blacklisted and whitelisted domains from various sources 
# across the internet, including the oficial Pi-hole discourse, diverse internet
# forums and media about ad-blocking.
# The lists are curated and upgraded from time to time.
# Project homepage: https://gitlab.com/brmsa/pihole-lists
# License terms: https://gitlab.com/brmsa/pihole-lists/blob/master/LICENSE
# Inspired by Anudeep's Commonly white listed domains for Pi-Hole project:
#   https://github.com/anudeepND/whitelist
# Designed to work with Pi-hole network wide ad-blocking
#   https://pi-hole.net/

# Define special characters/echo text formatting
TICK="[\e[32m ✔ \e[0m]"
TXTNC="\e[0m"
RED="\e[31m"
GREEN="\e[32m"
YELLOW="\e[33m"
BLUE="\e[34m"
MAGENTA="\e[35m"
CYAN="\e[35m"

echo -e "${YELLOW}This script will download aditional pihole adlists, blacklist and whitelist domains. Please wait... ${TXTNC} \n"
echo -e "${YELLOW}Project homepage: https://gitlab.com/brmsa/pihole-lists ${TXTNC}\n"
echo -e "${YELLOW}License terms: https://gitlab.com/brmsa/pihole-lists/blob/master/LICENSE ${TXTNC}\n"

# Check root privileges
if [ "$EUID" -ne 0 ]; then 
    echo -e "${RED}Please run as root ${TXTNC} \n"
    exit 126
else
    echo -e "${GREEN}User is root. Proceeding... ${TXTNC} \n"
fi

# Get adlist and merge with pihole lists
curl -sS https://gitlab.com/brmsa/pihole-lists/raw/master/lists/adlist.txt | sudo tee -a /etc/pihole/adlists.list > /dev/null
sleep 0.5
echo -e " ${TICK} ${GREEN}Adding providers to AD Lists... ${TXTNC}"
echo -e " ${TICK} ${GREEN}Removing duplicates... ${TXTNC}"
mv /etc/pihole/adlists.list /etc/pihole/adlists.list.old && cat /etc/pihole/adlists.list.old | uniq >> /etc/pihole/adlists.list
rm /etc/pihole/adlists.list.old

# Get blacklist and merge with pihole list
curl -sS https://gitlab.com/brmsa/pihole-lists/raw/master/lists/blacklist.txt | sudo tee -a /etc/pihole/blacklist.txt > /dev/null
sleep 0.5
echo -e " ${TICK} ${GREEN}Adding domains to blacklist... ${TXTNC}"
echo -e " ${TICK} ${GREEN}Removing duplicates... ${TXTNC}"
mv /etc/pihole/blacklist.txt /etc/pihole/blacklist.txt.old && cat /etc/pihole/blacklist.txt.old | uniq >> /etc/pihole/blacklist.txt
rm /etc/pihole/blacklist.txt.old

# Get RegEx blacklist and merge with pihole list
curl -sS https://gitlab.com/brmsa/pihole-lists/raw/master/lists/regex.txt | sudo tee -a /etc/pihole/regex.list > /dev/null
sleep 0.5
echo -e " ${TICK} ${GREEN}Adding domains to  RegEx blacklist... ${TXTNC}"
echo -e " ${TICK} ${GREEN}Removing duplicates... ${TXTNC}"
mv /etc/pihole/regex.list /etc/pihole/regex.list.old && cat /etc/pihole/regex.list.old | uniq >> /etc/pihole/regex.list
rm /etc/pihole/regex.list.old

# Get whitelist and merge with pihole list
curl -sS https://gitlab.com/brmsa/pihole-lists/raw/master/lists/whitelist.txt | sudo tee -a /etc/pihole/whitelist.txt > /dev/null
sleep 0.5
echo -e " ${TICK} ${GREEN}Adding commonly whitelisted domains to Pi-hole whitelist... ${TXTNC}"
echo -e " ${TICK} ${GREEN}Removing duplicates... ${TXTNC}"
mv /etc/pihole/whitelist.txt /etc/pihole/whitelist.txt.old && cat /etc/pihole/whitelist.txt.old | uniq >> /etc/pihole/whitelist.txt
rm /etc/pihole/whitelist.txt.old

# Update gravity
echo -e " [...] ${GREEN}Pi-hole gravity rebuilding lists. This may take a while... ${TXTNC}"
pihole -g
wait
echo -e " ${TICK} ${GREEN}Pi-hole's gravity updated ${TXTNC} \n"
echo -e " ${TICK} ${GREEN}Enjoy your newly updated Pi-hole! ${TXTNC}"
echo -e "\n\n"
