############################################
##wally3k ticked and non-crossed lists
############################################

##hpHosts Spam
https://hosts-file.net/grm.txt

##ReddestDream
https://reddestdream.github.io/Projects/MinimalHosts/etc/MinimalHostsBlocker/minimalhosts

##Fraud/adware/scam website
https://raw.githubusercontent.com/StevenBlack/hosts/master/data/KADhosts/hosts

##Spam sites based on hostsfile.org content.
https://raw.githubusercontent.com/StevenBlack/hosts/master/data/add.Spam/hosts

##Personal Blacklist by WaLLy3K
https://v.firebog.net/hosts/static/w3kbl.txt

##Spamassassin Blacklists
https://v.firebog.net/hosts/BillStearns.txt

##Cameleon ads blacklist
http://sysctl.org/cameleon/hosts

##This list blocks requests to sites which have used legal threats to remove themselves from other blacklists.
https://raw.githubusercontent.com/CHEF-KOCH/BarbBlock-filter-list/master/HOSTS.txt

##This list consists of Low Level Sensitivity website URLs
https://www.dshield.org/feeds/suspiciousdomains_Low.txt

##Domain blacklist base (spamvertised domains)
https://www.joewein.net/dl/bl/dom-bl-base.txt

##Community-contributed list of referrer spammers.
https://raw.githubusercontent.com/piwik/referrer-spam-blacklist/master/spammers.txt

##HostsFile.org blacklist
https://hostsfile.org/Downloads/hosts.txt

##Hosts file is brought to you by Dan Pollock
http://someonewhocares.org/hosts/zero/hosts

##Spam404 Lists
https://raw.githubusercontent.com/Dawsey21/Lists/master/main-blacklist.txt

##Vokins yhosts
https://raw.githubusercontent.com/vokins/yhosts/master/hosts

##Winhelp2002 MVPS
http://winhelp2002.mvps.org/hosts.txt

##AdAway default blocklist Blocking mobile ad providers and some analytics providers
https://adaway.org/hosts.txt

##Filter composed from several other filters (English filter, Social media filter, Spyware filter, Mobile Ads filter, EasyList and EasyPrivacy) and simplified specifically to be better compatible with DNS-level ad blocking
https://v.firebog.net/hosts/AdguardDNS.txt

##Ad filter list by Disconnect
https://s3.amazonaws.com/lists.disconnect.me/simple_ad.txt

##The following are hosts in the hpHosts database with the ATS classification ONLY
https://hosts-file.net/ad_servers.txt

##EasyList filter subscription
https://v.firebog.net/hosts/Easylist.txt

##A filter list to block all Spotify ads
https://raw.githubusercontent.com/CHEF-KOCH/Spotify-Ad-free/master/Spotifynulled.txt

##Windows installers ads sources sites based on https://unchecky.com/ content.
https://raw.githubusercontent.com/StevenBlack/hosts/master/data/UncheckyAds/hosts

##Airelle Trackers
https://v.firebog.net/hosts/Airelle-trc.txt

##EasyList filter subscription
https://v.firebog.net/hosts/Easyprivacy.txt

##Fabrice Prigent's Ads
https://v.firebog.net/hosts/Prigent-Ads.txt

##NoTrack is a network-wide DNS server which blocks Tracking sites
https://raw.githubusercontent.com/quidsup/notrack/master/trackers.txt

##2o7Net tracking sites based on hostsfile.org content.
https://raw.githubusercontent.com/StevenBlack/hosts/master/data/add.2o7Net/hosts

##Canvas Font fingerprinting pages filter list which gets an update one time per year
https://raw.githubusercontent.com/CHEF-KOCH/Canvas-Font-Fingerprinting-pages/master/Canvas.txt

##WebRTC tracking filter list
https://raw.githubusercontent.com/CHEF-KOCH/WebRTC-tracking/master/WebRTC.txt

##Audio fingerprint pages filter list which gets once time per year an update
https://raw.githubusercontent.com/CHEF-KOCH/Audio-fingerprint-pages/master/AudioFp.txt

##Canvas fingerprinting pages filter list LIST DEPRECATED BY CREATOR
https://raw.githubusercontent.com/CHEF-KOCH/Canvas-fingerprinting-pages/master/Canvas.txt

##This is a blocklist to block smart tv's sending meta data at home
https://raw.githubusercontent.com/Perflyst/PiHoleBlocklist/master/SmartTV.txt

##Airelle High Risk
https://v.firebog.net/hosts/Airelle-hrsk.txt

##Malvertising list by Disconnect
https://s3.amazonaws.com/lists.disconnect.me/simple_malvertising.txt

##DNS-BH Malware
https://mirror1.malwaredomains.com/files/justdomains

##The following are hosts in the hpHosts database with the EXP classification ONLY
https://hosts-file.net/exp.txt

##The following are hosts in the hpHosts database with the EMD classification ONLY
https://hosts-file.net/emd.txt

##The following are hosts in the hpHosts database with the PSH classification ONLY
https://hosts-file.net/psh.txt

##This is a list of long-lived, "immortal", malware domains
https://mirror.cedia.org.ec/malwaredomains/immortal_domains.txt

##MalwareDomainList.com Hosts List
https://www.malwaredomainlist.com/hostslist/hosts.txt

##Mandiant APT1 Report
https://bitbucket.org/ethanr/dns-blacklists/raw/8575c9f96e5b4a1308f2f12394abd86d0927a4a0/bad_lists/Mandiant_APT1_Report_Appendix_D.txt

##Fabrice Prigent's Malware
https://v.firebog.net/hosts/Prigent-Malware.txt

##Fabrice Prigent's Phishing
https://v.firebog.net/hosts/Prigent-Phishing.txt

##NoTrack Malicious Site list
https://raw.githubusercontent.com/quidsup/notrack/master/malicious-sites.txt

##Ransomware Domain Blocklist
https://ransomwaretracker.abuse.ch/downloads/RW_DOMBL.txt

##Shalla's Blacklists is a collection of URL lists grouped into several categories intended for the usage with URL filters
https://v.firebog.net/hosts/Shalla-mal.txt

##Risk content sites based on hostsfile.org content.
https://raw.githubusercontent.com/StevenBlack/hosts/master/data/add.Risk/hosts

##abuse.ch ZeuS domain blocklist
https://zeustracker.abuse.ch/blocklist.php?download=domainblocklist

##Pr0n blocker list (DISABLE IT BY DEFAULT, FOR FUCK'S SAKE!
#https://raw.githubusercontent.com/chadmayfield/my-pihole-blocklists/master/lists/pi_blocklist_porn_all.list

##Pr0n blocker list (DISABLE IT BY DEFAULT, FOR FUCK'S SAKE!
#https://raw.githubusercontent.com/chadmayfield/pihole-blocklists/master/lists/pi_blocklist_porn_top1m.list

##############################################
##CryptoAUSTRALIA's Favourite Block Lists
##############################################

##Locky Ransomware C2 domain blocklist
https://ransomwaretracker.abuse.ch/downloads/LY_C2_DOMBL.txt

##CryptoWall Ransomware C2 domain blocklist
https://ransomwaretracker.abuse.ch/downloads/CW_C2_DOMBL.txt

##TeslaCrypt Ransomware C2 domain blocklist
https://ransomwaretracker.abuse.ch/downloads/TC_C2_DOMBL.txt

##TorrentLocker Ransomware C2 domain blocklist
https://ransomwaretracker.abuse.ch/downloads/TL_C2_DOMBL.txt

##DShield.org Suspicious Domain List
https://isc.sans.edu/feeds/suspiciousdomains_Medium.txt

##Domain blacklist
http://www.joewein.net/dl/bl/dom-bl.txt

##Block spying and tracking on Windows
https://github.com/crazy-max/WindowsSpyBlocker/blob/master/data/hosts/spy.txt

##Blocks online functions of Windows
#https://github.com/crazy-max/WindowsSpyBlocker/blob/master/data/hosts/extra.txt

##Blocks Windows Updates servers
#https://github.com/crazy-max/WindowsSpyBlocker/blob/master/data/hosts/update.txt

##Samsung SmartTV domains (Will block "Smart" aspects of device)
https://v.firebog.net/hosts/static/SamsungSmart.txt

####################################################################
##Some lists of adserving and tracking sites maintained by anudeepND
###################################################################

##Host file to block crypto currency mining sites
https://raw.githubusercontent.com/anudeepND/blacklist/master/CoinMiner.txt

##Host file to block ads, tracking and more!
https://raw.githubusercontent.com/anudeepND/blacklist/master/adservers.txt

##A list of YouTube video ad domains. (EXPERIMENTAL!)
https://raw.githubusercontent.com/anudeepND/youtubeadsblacklist/master/hosts.txt

######################################
#Lists to block CoinMiners by ZeroDot1
######################################

##This list contains all domains - A hostslist for administrators to prevent mining in networks
https://zerodot1.gitlab.io/CoinBlockerLists/hosts

##This list contains all browser mining domains - A hostslist to prevent browser mining only
https://zerodot1.gitlab.io/CoinBlockerLists/hosts_browser

##This list contains all optional domains - An additional hostslist for administrators
https://zerodot1.gitlab.io/CoinBlockerLists/hosts_optional

#############################
##Other Blacklists
#############################

##HOSTS file to block all known NSA / GCHQ / C.I.A. / F.B.I. spying servers
https://raw.githubusercontent.com/CHEF-KOCH/NSABlocklist/master/HOSTS

##To Completely Block Facebook
#https://raw.githubusercontent.com/jmdugan/blocklists/master/corporations/facebook/all