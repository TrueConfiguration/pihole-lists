# Pi-hole additional lists

Adds aditional lists for Pi-hole.

The script will download and add to the configuration files a selection of adlists, commonly blacklisted and whitelisted domains from various sources across the internet.